# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hash = {}
  str.split.each do |word|
    hash[word] = word.length
  end
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  largest = nil
  largestkey = nil
  hash.each do |k,v|
    if largest == nil || v > largest
      largest = v
      largestkey = k
    end
  end
  largestkey
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = Hash.new(0)
  word.split('').each do |l|
    letters[l] += 1
  end
  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = {}
  arr.each_with_index do |el, index|
    if hash.include?(el) == false
      hash[el] = index
    end
  end
  newarr = []
  hash.each do |k,v|
    newarr.push(k)
  end
  newarr
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = {odd: 0, even: 0}
  numbers.each do |el|
    if el%2 == 0
      hash[:even] += 1
    else
      hash[:odd] += 1
    end
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowelhash = Hash.new(0)
  vowels = ['a','e','i','o','u']
  string.split('').each do |l|
    if vowels.include?(l)
      vowelhash[l] += 1
    end
  end
  vowelhash.sort_by {|k,v| v}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  array = []
  students.each do |k,v|
    if v>6
      array.push(k)
    end
  end
  array.combination(array.length-1)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.length
  smallest_population_size = nil
  largest_population_size = nil
  specimens.each do |el|
    if smallest_population_size == nil || specimens.count(el) < smallest_population_size
      smallest_population_size = specimens.count(el)
    end
    if largest_population_size == nil || specimens.count(el) > largest_population_size
      largest_population_size = specimens.count(el)
    end
  end
  return (number_of_species**2) * (smallest_population_size/largest_population_size)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  v_hash = character_count(punc_deleter(vandalized_sign))
  n_hash = character_count(punc_deleter(normal_sign))
  n_hash.each do |k,v|
    if vandalized_sign.count(k) > v 
      return false
    end
  end
  return true
end

def character_count(str)
  counter = Hash.new(0)
  str.split('').each do |chr|
    counter[chr] += 1
  end
  counter
end

def punc_deleter(str)
  str = str.downcase
  alphabet = 'a'.upto('z').to_a
  array = []
  str.split('').each do |chr|
    if alphabet.include?(chr)
      array.push(chr)
    end
  end
  array.join('')
end
